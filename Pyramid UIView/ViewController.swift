//
//  ViewController.swift
//  Pyramid UIView
//
//  Created by Volodymyr on 4/25/19.
//  Copyright © 2019 Volodymyr. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        // три квадрата в линию
        // drawBox(3)
        // лесенка
        ladderDrawBox(3)
       // piramidBox(3)
    }
    
    func drawBox(_ quantityOfBox: Int) {
        var xBias = 27
        for i in 1...quantityOfBox {
            let box = UIView()
            box.frame.size.width = 70
            box.frame.size.height = 70
            
            box.frame.origin.x = CGFloat(xBias)
            box.frame.origin.y = 496
            
            box.backgroundColor = .yellow
            view.addSubview(box)
            xBias += 97
        }
        
    }
    
    func ladderDrawBox(_ quantityOfBox: Int) {
        var xBias = 27
        var ladderBox = 0
        for i in 1...quantityOfBox {
            if i == 2 || i == 3 {
                xBias += 97
                ladderBox = i - 1
            }
            var yBias = 496
            for j in 1...quantityOfBox - ladderBox {
                let box = UIView()
                box.layer.cornerRadius = 35
                box.frame.size.width = 70
                box.frame.size.height = 70
                box.frame.origin.x = CGFloat(xBias)
                box.frame.origin.y = CGFloat(yBias)
                box.backgroundColor = .yellow
                view.addSubview(box)
                yBias -= 97
            }
        }
    }
    
    func piramidBox(_ quantityOfBox: Int) {
        var xBias = 27
        var ladderBox = 0
        var yBias = 496
        for i in 1...quantityOfBox {
            
            if i == 2 {
                yBias -= 97
                ladderBox = i - 1
                xBias = 77
            }
            if i == 3 {
                yBias -= 97
                ladderBox = i - 1
                xBias = 125
            }
            
            for j in 1...quantityOfBox - ladderBox {
                let box = UIView()
                box.layer.cornerRadius = 35
                box.frame.size.width = 70
                box.frame.size.height = 70
                box.frame.origin.x = CGFloat(xBias)
                box.frame.origin.y = CGFloat(yBias)
                box.backgroundColor = .yellow
                view.addSubview(box)
                xBias += 97
            }
        }
    }
}
